FROM java:8-jdk-alpine
ARG JAR_FILE
COPY ${JAR_FILE} authsvc.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom", "-jar", "/authsvc.jar"]

