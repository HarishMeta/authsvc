package io.bhishma.auth.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import io.bhishma.auth.entity.UserEntity;

public interface UserRepository extends CrudRepository<UserEntity, Long>{
	
	List<UserEntity> findByEmail(String email);

}
