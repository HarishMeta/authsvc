package io.bhishma.auth.service;

import io.bhishma.platform.boot.user.User;

public interface UserService {
	
	void create(User user);
	User login(User user);

}
