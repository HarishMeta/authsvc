package io.bhishma.auth.service;

import java.util.List;

import io.bhishma.auth.AuthResponse;
import io.bhishma.platform.boot.user.User;

public interface TokenService {
	
	List<String> getTokens();
	AuthResponse verifyToken(String token);
	void remove(String token);
	String generateToken(User user);
	User getUserByToken(String token);
	
}
