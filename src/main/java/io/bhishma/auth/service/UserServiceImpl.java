package io.bhishma.auth.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import io.bhishma.auth.entity.UserEntity;
import io.bhishma.auth.repository.UserRepository;
import io.bhishma.platform.boot.user.User;

@Component
public class UserServiceImpl implements UserService {
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	private UserRepository userRepository;

	@Override
	public void create(User user) {
		final UserEntity userEntity= convert(user);
		userRepository.save(userEntity);
	}
	
	@Override
	public User login(User user) {
		
		final List<UserEntity> users = userRepository.findByEmail(user.getEmail());
		
		if(!CollectionUtils.isEmpty(users)) {
			final UserEntity userEntity = users.get(0);
			if(passwordEncoder.matches(user.getPassword(), userEntity.getPassword())) {
				return convertToUser(userEntity);
			}

		}
		return null;
	}
	
	private UserEntity convert(User user) {
		
		UserEntity ue = new UserEntity();
		ue.setEmail(user.getEmail());
		ue.setName(user.getName());
		ue.setPassword(passwordEncoder.encode(user.getPassword()));
		ue.setUserName(user.getUsername());
		return ue;
		
	}
	
	private User convertToUser(UserEntity userEntity) {
		
		User ue = new User();
		ue.setEmail(userEntity.getEmail());
		ue.setName(userEntity.getName());
		ue.setId(userEntity.getId());
		
		return ue;
		
	}

}
