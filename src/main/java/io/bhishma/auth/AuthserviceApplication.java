package io.bhishma.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import io.bhishma.platform.boot.config.CorePlatformInitializer;

@SpringBootApplication
public class AuthserviceApplication extends CorePlatformInitializer {

	public static void main(String[] args) {
		SpringApplication.run(AuthserviceApplication.class, args);
	}

}
