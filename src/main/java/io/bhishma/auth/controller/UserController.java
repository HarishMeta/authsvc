package io.bhishma.auth.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.bhishma.auth.service.UserService;
import io.bhishma.platform.boot.user.User;

@RestController
@RequestMapping("/api/v1/auth/user")
public class UserController {
	
	@Autowired
	private UserService userService;
	
	
	@PostMapping
	public ResponseEntity<Object> create(@RequestBody final User user) {
		userService.create(user);
		user.setPassword(null);
		return new ResponseEntity<Object>(user, HttpStatus.OK);		
	}
	
	

}
